*** Settings ***
Resource          resource.robot
Suite Teardown    Fechar Navegador


*** Variables ***

${navegador}          chrome
${link}               https://amazon.com.br/
${campo_pesquisar}    xpath=//*[@id="twotabsearchtextbox"]
${botão_pesquisar}    xpath=//*[@id="nav-search-submit-button"]
${item}               xpath=//*[@id="search"]/div[1]/div[1]/div/span[1]/div[1]/div[3]/div/div/div/div/div/div/div[1]/div/span/a/div
${add_carrinho}       xpath=//*[@id="add-to-cart-button"]
${carrinho}           xpath=//*[@id="nav-cart-count-container"]
${item_carrinho}      xpath=//*[@class='a-truncate-full a-offscreen']




*** Test Cases ***

Cenário 01 - abrir site site da amazon
    Open Browser                        ${link}               ${navegador}
    Maximize Browser Window
    Input Text                          ${campo_pesquisar}    sapatos
    Click Button                        ${botão_pesquisar}
    Sleep                               3s
    Capture Page Screenshot
    Wait Until Element Is Visible       ${item}
    Click Element                       ${item}
    Sleep                               3s
    Capture Page Screenshot
    Wait Until Element Is Visible       ${add_carrinho}
    Click Button                        ${add_carrinho}
    Sleep                               3s
    Capture Page Screenshot
    Wait Until Element Is Visible       ${carrinho}
    Click Element                        ${carrinho}
    Sleep                               3s
    Capture Page Screenshot
    ${nome_item}    Get Text    ${item_carrinho}
    Element Text Should Be    ${nome_item}    Asyusyu Sapatos de jazz Couro PU Sapatos de dança Chinelo com elásticos Moderno Sapatos de dança para crianças-Preto,30BR
    